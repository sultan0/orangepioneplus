#!/usr/bin/env sh

### setup screen resolution to 1920x1080
# to get the screen info type:
# gtf 1920 1080 60.0
# copy the output and use it as per the following


xrandr --newmode "1920x1080_60.00"  172.80  1920 2040 2248 2576  1080 1081 1084 1118  -HSync +Vsync
xrandr --addmode HDMI-1 1920x1080_60.00
xrandr --output HDMI-1 --mode 1920x1080_60.00

