#!/usr/bin/env sh

# This script to install i3 on Orange Pi One plus (H6)
# owner: Sultan
# created date : 13 Aug 2020
cecho() {
  local code="\033["
  case "$1" in
    black  | bk) color="${code}0;30m";;
    red    |  r) color="${code}1;31m";;
    green  |  g) color="${code}1;32m";;
    yellow |  y) color="${code}1;33m";;
    blue   |  b) color="${code}1;34m";;
    purple |  p) color="${code}1;35m";;
    cyan   |  c) color="${code}1;36m";;
    gray   | gr) color="${code}0;37m";;
    *) local text="$1"
  esac
  [ -z "$text" ] && local text="$color$2${code}0m"
  echo "$text"
}


#
# update 
cecho g " Updating source ...."
sudo apt update

### installing main packages ###
cecho g " Installing main packages ...."
sudo apt install -y xorg
sudo apt install -y python3-pip
sudo apt install -y i3 i3blocks i3status
sudo apt install -y suckless-tools
sudo apt install -y rxvt-unicode 
sudo apt install -y rxvt-unicode-256color rxvt-perls libclipboard-perl
sudo apt install -y w3m-img feh
sudo apt install -y vim
sudo apt install -y zip unzip
sudo apt install -y zsh tmux
sudo apt install -y make
sudo apt install -y rofi

### installing images util
sudo apt install -y imagemagick
sudo apt install -y scrot

### installing fireforx
cecho g "Installing FireFox"
sudo apt install -y firefox-esr

### installing file managers and utils tools
cecho g "Installing file mangers and utils tools ..."
sudo apt install -y git wget ftp gvfs curl
sudo apt install -y ranger
sudo apt install -y pcmanfm


#### install oh-my-zh and make zsh defualt 
cecho g "Installing oh-my-zh and make zsh defualt ..." 
wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh
chmod +x install.sh
sh install.sh --unattended
command -v zsh | sudo tee -a /etc/shells
sudo chsh -s "$(command -v zsh)" "${USER}"

#chsh -s $(which zsh)

## install required python3 libraries ## these are optinal 
cecho g "Installing python3 libraries ...."
sudo apt install -y python3-numpy python3-pandas python3-pillow python3-xlib
sudo apt install -y python3-setuptools python3-psutil
pip3 install requests
pip3 install blessings

# setup ranger
# under process

#### install broot
# wget https://dystroy.org/broot/download/armv7-unknown-linux-gnueabihf/broot
# sudo mv broot /usr/local/bin
# sudo chmod +x /usr/local/bin/broot
# broot

## Final stage install configration files
##   ( this part is optional )


install_config(){

    #### downloading the all files
    git clone https://gitlab.com/sultan0/orangepioneplus.git
    cd orangepioneplus
    
    #### install precombiled i3-gaps
    cecho g "Installing i3-gaps"
    sudo dpkg -i --force-overwrite build_4.18.02-1_arm64.deb

    ##### installing dotfiles
    cecho g "Installing config dotfiles ..."    
    cp dotfiles/i3-config $HOME/.config/i3/config
    cp dotfiles/.Xresources $HOME/.Xresources
    cp dotfiles/.bashrc $HOME/.bashrc
    cp dotfiles/.profile $HOME/.profile
    cp dotfiles/.vimrc $HOME/.vimrc
    cp dotfiles/.xinitrc $HOME/.xinitrc
    cp dotfiles/.zshrc $HOME/.zshrc
    if [ ! -d  $HOME/.config/rofi/ ]; then
       mkdir $HOME/.config/rofi/
    fi
    cp dotfiles/rofi-power $HOME/.config/rofi/power.sh
    chmod +x $HOME/.config/rofi/power.sh


    ###### adding wall paperi
    cecho g "Installing wallpaper ..."
    if [ ! -d  $HOME/.walls/ ]; then
        mkdir $HOME/.walls/
    fi
    cp wall.jpg $HOME/.walls/

    # installing fonts
    cecho g "Installing fonts ...."
    if [ ! -d  $HOME/.fonts/ ]; then
        mkdir $HOME/.fonts/
    fi
    unzip -o -d $HOME/ dotfiles/fonts.zip
    
    # adding .local/bin 
    if [ ! -d $HOME/.local/bin ]; then
        mkdir $HOME/.local/bin/
    fi
    cp screen.sh $HOME/.local/bin/
    cp screen_res.sh $HOME/.local/bin/
    #Finishing note:
    cecho y "Note:"
    cecho y "To change wallpaper just rename the jpg file to $HOME/.walls/wall.jpg."
    cecho y "Example : mv <your.jpg> $HOME/.walls/wall.jpg"

}

while true; do
    read -p "Do you wish to install configration files?" yn
    case $yn in
        [Yy]* ) install_config; break;;
        [Nn]* ) exit;;
        * ) cecho r "Please answer yes or no.";;
    esac
done

cecho y "****** enjoy ******"

