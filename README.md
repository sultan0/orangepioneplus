# Orange Pi One Plus 
## installing i3

**Installing i3wm on armbian. On Orange Pi One Plus (H6).**


### Note : this is tested on Armbian buster with Linux 5.4.43-sunxi64 


This include installation script ( assuming armbian already installed ):
- installation scripts.
- dotfiles.

**Installing**

```
wget https://gitlab.com/sultan0/orangepioneplus/-/raw/master/install_i3_OPOP.sh

```
```
chmod +x install_i3_OPOP.sh
./install_i3_OPOP.sh
```
Once installation is over logoff and then login and type :

```
startx
```

* To change screen resolution you can edit and run "screen_res.sh".
* This installation will install some configration that I use and you may change them any time.
