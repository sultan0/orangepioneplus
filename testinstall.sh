

install_config(){

    #### downloading the all files
    git clone https://gitlab.com/sultan0/orangepioneplus.git
    cd orangepioneplus
    
    #### install precombiled i3-gaps
    #sudo dpkg -i --force-overwrite build_4.18.02-1_arm64.deb

    ##### installing dotfiles    
    cp dotfiles/i3-config $HOME/.config/i3/config
    cp dotfiles/.Xresources $HOME/.Xresources
    cp dotfiles/.bashrc $HOME/.bashrc
    cp dotfiles/.profile $HOME/.profile
    cp dotfiles/.vimrc $HOME/.vimrc
    cp dotfiles/.xinitrc $HOME/.xinitrc
    cp dotfiles/.zshrc $HOME/.zshrc
    if [ ! -d  $HOME/.config/rofi/ ]; then
       mkdir $HOME/.config/rofi/
    fi
    cp dotfiles/rofi-power $HOME/.config/rofi/power.sh
    chmod +x $HOME/.config/rofi/power.sh


    ###### adding wall paper
    if [ ! -d  $HOME/.walls/ ]; then
        mkdir $HOME/.walls/
    fi
    cp wall.jpg $HOME/.walls/

    # installing fonts
    if [ ! -d  $HOME/.fonts/ ]; then
        mkdir $HOME/.fonts/
    fi
    unzip -o -d $HOME/.fonts/ dotfiles/fonts.zip
}

while true; do
    read -p "Do you wish to install configration files?" yn
    case $yn in
        [Yy]* ) install_config; break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done
